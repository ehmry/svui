# Syndicate Vector User Interfaces

[Syndicate](https://syndicate-lang.org/) [schema](./svui.prs), bindings, and
some rendering modules.
