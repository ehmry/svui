import std/[typetraits, unittest]
import preserves
import syndicate/patterns
import svui

type Svui = svui.Svui[void]

suite "patterns":
  test "grab-all":
    let pat = Svui ? {0: grab(), 1: grab()}
    check $pat == "<compound <rec svui 2> {0: <bind <_>>, 1: <bind <_>>}>"
