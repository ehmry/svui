
import
  std/typetraits, preserves, std/tables, std/tables, std/tables, std/tables

type
  XmlNodeKind* {.pure.} = enum
    `XmlElement`, `XmlText`
  `XmlNode`* {.preservesOr.} = object
    case orKind*: XmlNodeKind
    of XmlNodeKind.`XmlElement`:
        `xmlelement`*: XmlElement

    of XmlNodeKind.`XmlText`:
        `xmltext`*: XmlText

  
  XmlText* {.preservesTuple.} = object
    `data`*: string

  Svui*[E] {.preservesRecord: "svui".} = ref object
    `attrs`*: Table[string, Preserve[E]]
    `data`*: Content

  XmlElement* {.preservesTuple.} = object
    `name`* {.preservesSymbol.}: string
    `attrs`*: Table[string, string]
    `elements`* {.preservesTupleTail.}: seq[XmlNode]

  Svg* {.preservesTuple.} = object
    `data`* {.preservesLiteral: "svg".}: bool
    `attrs`*: Table[string, string]
    `elements`* {.preservesTupleTail.}: seq[XmlNode]

  Xhtml* {.preservesTuple.} = object
    `data`* {.preservesLiteral: "html".}: bool
    `attrs`*: Table[string, string]
    `elements`* {.preservesTupleTail.}: seq[XmlNode]

  ContentKind* {.pure.} = enum
    `Svg`, `Xhtml`
  `Content`* {.preservesOr.} = object
    case orKind*: ContentKind
    of ContentKind.`Svg`:
        `svg`*: Svg

    of ContentKind.`Xhtml`:
        `xhtml`*: Xhtml

  
proc `$`*[E](x: Svui[E]): string =
  `$`(toPreserve(x, E))

proc encode*[E](x: Svui[E]): seq[byte] =
  encode(toPreserve(x, E))

proc `$`*(x: XmlNode | XmlText | XmlElement | Svg | Xhtml | Content): string =
  `$`(toPreserve(x))

proc encode*(x: XmlNode | XmlText | XmlElement | Svg | Xhtml | Content): seq[
    byte] =
  encode(toPreserve(x))
