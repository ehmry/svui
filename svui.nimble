# Package

version       = "0.1.0"
author        = "Emery Hemingway"
description   = "Syndicate Vector User Interfaces"
license       = "Unlicense"
srcDir        = "nim"


# Dependencies

requires "nim >= 1.6.0"
